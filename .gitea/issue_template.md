---

name: "New proposal"
about: "Use this template when you want to propose a new project."
title: ""

---

# Problem
Please give a brief introduction into the problem you want to solve.

# Solution
If you already have some ideas about a possible solution, this is the right place to tell us. :)

# Further Ideas
- Idea 1
- Idea 2

# Existing (related) projects
If there are already some existing projects out there that face a similar problem or might give some inspiration, put a link to these projects here and optionally, describe why they do not fulfull your needs. These related projects might also be proprietary services and therefore do not put the users in control.

# Mockups
If you already have created some mockups, this is the right place for them. Otherwise, just add a todo here.